# TSP_Project

Zadanie polega na optymalizacji drogi pomiędzy wygenerowanymi miastami. Można je wykonać:
* W grupie (_zalecane_)
* Samodzielnie

Należy zaimplementować minimalnie 3 funkcje (można się podzielić pracą własnie wg. tych zadań:
* get_distances

Funkcja ta powinna zwracać macierz z dystansami pomiędzy poszczególnymi miastami.
Na sam początek proponuję zwrócić chociażby odpowiednią macierz wypełnioną jedynkami, tak aby następna osoba miała na czym testować rozwiązanie ;) 

`result = (np.ones(len(cities), len(cities)))`

Naiwne podejście może korzystać z pitagorasa, ale wskazane jest wskazanie realnego distansu pomiędzy miastami
W tym celu można wykorzystać bibliotekę geopy

* plan_route

Plan_route można wykonać za pomocą wielu algorytmów.
Proponuję rozpocząc od losowości tak, żeby kolejna osoba mogła już sobie działać
Inne algorytmy:
* BruteForce (przeszukanie zupełne)
* Random (Szukanie n czasu wśród losowych wyników - zwrot najlepszego)
* Symulowane wyżarzanie (mam nadzieję, że nie strzeliłem tutaj orta xd)
* Algorytm genetyczny

Polecam porównać kilka algorytmów (jeżeli starczy czasu)

* visualize

Prosta wizualizacja na zwykłym wykresie (x,y)
Wizualizacja na mapie Polski (hard)


