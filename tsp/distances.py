from typing import List
from .town_generator import City
import numpy as np
from geopy.distance import geodesic


def get_distances(lst_of_cities: List[City]) -> np.ndarray:
    """
    Calculates the distance between cities.
    :param lst_of_cities: list of City objects
    :return: numpy.ndarray consisting of distances between individual cities
    """
    pairs = set()

    num_cities = len(lst_of_cities)

    result = np.ndarray(shape=(num_cities, num_cities))

    for x, city_a in enumerate(lst_of_cities):
        for y, city_b in enumerate(lst_of_cities):

            if city_a == city_b:
                continue

            name_alpha = str(sorted((city_a.name, city_b.name)))

            if name_alpha in pairs:
                continue

            pairs.add(name_alpha)

            coordinate1 = (city_a.latitude, city_a.longitude)
            coordinate2 = (city_b.latitude, city_b.longitude)
            distance_in_km = geodesic(coordinate1, coordinate2).km

            result[x, y] = distance_in_km
            result[y, x] = distance_in_km

    return result


