"""
Python function to visualize route between cities, based on a list of City objects
"""
from typing import List
import gmplot
from .town_generator import City


def visualize(cities: List[City]):
    """
    Visualize route on Google Maps using library gmplot.
    :param cities: list of City objects.
    :return: creates an html file with the route based on latitudes and longitudes of City objects.
    """
    latitudes = []
    longitudes = []
    for city in cities:
        latitudes.append(city.latitude)
        longitudes.append(city.longitude)
    my_map = gmplot.GoogleMapPlotter(latitudes[0], longitudes[0], 6)
    my_map.scatter(latitudes, longitudes, '#ff0000', size=50, marker=False)
    my_map.plot(latitudes, longitudes, '#ff0000', edge_width=1)
    for i in cities:
        index = cities.index(i)
        my_map.text(latitudes[index], longitudes[index], str(index + 1), color='yellow')
        my_map.marker(latitudes[index], longitudes[index], title=i.name, color='yellow',
                      symbol='x', label=i.name[0], info_window=f'<a href=\'#\'>{i.name}</a>')
    my_map.draw("map.html")
