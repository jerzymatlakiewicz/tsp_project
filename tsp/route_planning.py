import random
import time
from dataclasses import dataclass

import numpy as np
from typing import List

from tqdm import tqdm

from .town_generator import City
from random import choice

import matplotlib.pyplot as plt



def plan_route_monte_carlo(cities: List[City], distances: np.ndarray, iterations: int = 10000) -> List[City]:
    """
    Optimize route by the Monte Carlo method.
    :param cities: a list of City objects in any order
    :param distances: an array of distances (as integers) between the cities in <cities>, where element
                      at the index [2,5] corresponds to the distance between cities at indices 2 and 5
                      in the <cities> list
    :param iterations: number of routes drawn in the process. The higher <iterations> value, the bigger
                       the accuracy of the optimization
    :return: optimized route between the cities in the <cities> list
    """
    if (len(cities), len(cities)) != distances.shape:
        raise ValueError('Sizes of city list and distances array do not match.')
    route_data = {}
    for _ in range(iterations):
        drawn_route = draw_route(cities)
        total_distance = 0
        last_city_index = cities.index(drawn_route[0])
        for city in drawn_route:
            current_city_index = cities.index(city)
            total_distance += distances[last_city_index, current_city_index]
            last_city_index = current_city_index
        route_data[total_distance] = drawn_route
    return route_data[min(route_data)]


def draw_route(cities: List[City]) -> List[City]:
    """
    Draw randomly a single route between the cities in the list taken as the parameter.
    :param cities: a list of City objects in any order
    :return: random route between the cities given in parameter
    """
    cities_copy = cities.copy()
    result = []
    while cities_copy:
        drawn_city = choice(cities_copy)
        result.append(drawn_city)
        cities_copy.remove(drawn_city)
    return result


@dataclass
class Route:
    cities: List[City]
    distance: float


class GeneticAlgorithmTSP:

    def __init__(self, population: int = 100, elites: int = 3, mutations: int = 5):
        self.cities = None
        self.distances = None
        self.genotype_length = None
        self.population = population
        self.elites = elites
        self.mutations = mutations
        self.history = []

    def fit(self, cities: List[City], distances: np.ndarray, timeout) -> List[City]:
        self.cities = cities
        self.distances = distances
        self.genotype_length = len(cities)
        actual_population = [self._generate_random_route() for _ in range(self.population)]
        actual_population.sort(key=lambda x: x.distance)
        self.history.append(actual_population[0].distance)

        start_time = time.time()
        elapsed_time = 0

        progress_bar = tqdm(total=timeout)

        while elapsed_time < timeout:
            actual_population = self._new_population(actual_population)
            self.history.append(actual_population[0].distance)
            previous_loop_time, elapsed_time = elapsed_time, time.time() - start_time
            progress_bar.update(elapsed_time - previous_loop_time)

        print(actual_population[0].distance)

        plt.plot(range(len(self.history)), self.history)
        plt.show()
        return actual_population[0].cities

    def _get_distance(self, route):
        return sum([self.distances[city1.index, city2.index] for city1, city2 in zip(route[:-1], route[1:])])

    def _generate_random_route(self):
        new_route = random.sample(self.cities, len(self.cities))
        dist = self._get_distance(new_route)

        return Route(new_route, dist)

    def _new_population(self, population):
        new_pop = population[:self.elites]
        new_pop.extend([self._generate_random_route() for _ in range(self.mutations)])
        new_pop.extend([self._crossing(population[int(min(abs(random.normalvariate(0, sigma=len(population)/25)),
                                                          len(population)-1))],
                                       population[int(min(abs(random.normalvariate(0, sigma=len(population)/25)),
                                                          len(population)-1))])
                        for _
                        in range(len(population)-len(new_pop))])

        new_pop.sort(key=lambda x: x.distance,)
        return new_pop

    def _crossing(self, first: Route, second: Route):
        start = random.randint(0, self.genotype_length)
        end = random.randint(0, self.genotype_length)

        start_gene = min(start, end)
        end_gene = max(start, end)

        new_genome_fragment = first.cities[start_gene:end_gene]

        child_cities = list(set(second.cities) - set(new_genome_fragment))
        child_cities[start_gene:start_gene] = new_genome_fragment

        child = Route(child_cities, self._get_distance(child_cities))
        return child


